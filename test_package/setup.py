#!/usr/bin/python3

from setuptools import setup
import os

setup(
    name='test_package',
    version=os.environ.get("VERSION"),    
    description='A simple test Python package',
    url='https://bcbswny.com',
    author='Tobias Riazi',
    author_email='riazi.tobias@healthnow.org',
    license='BSD 2-clause',
    packages=['test_package'],
    # install_requires=['mpi4py>=2.0',
    #                   'numpy',                     
    #                   ],

     classifiers=[
         'Development Status :: 1 - Planning',
    #     'Intended Audience :: Science/Research',
    #     'License :: OSI Approved :: BSD License',  
         'Operating System :: POSIX :: Linux',        
    #     'Programming Language :: Python :: 2',
    #     'Programming Language :: Python :: 2.7',
    #     'Programming Language :: Python :: 3',
    #     'Programming Language :: Python :: 3.4',
    #     'Programming Language :: Python :: 3.5',
     ],
)
